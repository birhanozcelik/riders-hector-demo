#!/bin/sh

sudo apt update

sudo apt install -y ros-melodic-hector-mapping \
                  ros-melodic-geographic-msgs \
                  ros-melodic-unique-id \
                  ros-melodic-hector-sensors-description \
                  ros-melodic-teleop-twist-keyboard

rosdep install --rosdistro $ROS_DISTRO --ignore-src --from-paths src -y