# riders-hector-demo

Riders platformunda Hector drone icin ilk kurulum ve ornek kullanim detaylari tasiyan bir paket.

## Demoyu baslatma


1. **Ctrl+`** ile terminal acip, 

```
cd /workspace/src/_lib/riders-hector-demo/
```
ile pakate gidin ve `initial_setup.sh`'i calistirin

```
chmod +x initial_setup.sh
./initial_setup.sh
```

2. Sonra `workspace` klasorune gidip projeyi kurun

```
cd /workspace
catkin_make
```

3. Simulasyonu `Start` ile baslatabilirsiniz.
 
4. **Ctrl+`** ile terminal acip, once drone motorlarini aktif hale getiren servisi cagiralim:

```
rosservice call /enable_motors "enable: true"
```

5. Sonra keyboard teleoperation paketini calistirip baslamaya hazir hale gelebiliriz:

```
rosrun teleop_twist_keyboard teleop_twist_keyboard.py
```

